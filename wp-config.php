<?php
/** Enable W3 Total Cache */
define('WP_CACHE', true); // Added by W3 Total Cache

$root = $_SERVER['HTTP_HOST'];
$local_roots = array('igraonica.nikola.lan', 'igraonica.zax.lan');
$dev_roots = array('igraonica.uizradi.com');
$production_roots = array('www.igraoni.ca');

if( in_array($root, $dev_roots) )
{
    // Dev
    define('WP_ENV', 'development');
    define('DB_NAME', 'igraonica');
    define('DB_USER', 'nikolag');
    define('DB_PASSWORD', 'nezaboravime1000$');
    define('DB_HOST', 'localhost');
    define('DB_CHARSET', 'utf8mb4');
    define('DB_COLLATE', '');
    define('DOMAIN_CURRENT_SITE', 'http://igraonica.uizradi.com');
    @ini_set( 'log_errors', 'on' );
    @ini_set( 'display_errors', 'on' );
    define('FS_METHOD', 'direct');
    define('WP_DEBUG', true);
    define('WP_DEBUG_LOG', false);
    define('WP_DEBUG_DISPLAY', false);

}
elseif (in_array($root, $production_roots))
{
    // Prod
    define('WP_ENV', 'production');
    define('AMS_ENV', 'production');
    define('DB_NAME', 'igraonica_3_production');
    define('DB_USER', 'nikolag');
    define('DB_PASSWORD', 'nezaboravime1000$');
    define('DB_HOST', 'localhost');
    define('DB_CHARSET', 'utf8mb4');
    define('DB_COLLATE', '');
    define('DOMAIN_CURRENT_SITE', 'https://www.igraoni.ca');
    define('FS_METHOD', 'direct');
    define('WP_DEBUG', false);
    define('WP_DEBUG_LOG', false);
    define('WP_DEBUG_DISPLAY', false);
}
else
{
    if ($root == 'igraonica.nikola.lan')
    {
        // nikola env
        define('WP_ENV', 'local_development');
        define('DB_NAME', 'igraonica');
        define('DB_USER', 'homestead');
        define('DB_PASSWORD', 'secret');
        define('DB_HOST', 'localhost');
        define('DB_CHARSET', 'utf8mb4');
        define('DB_COLLATE', '');
        define('DOMAIN_CURRENT_SITE', 'http://igraonica.nikola.lan');
        define('WP_DEBUG', true);
        define( 'ALLOW_UNFILTERED_UPLOADS', true );
    }
    elseif ($root == 'igraonica.zax.lan')
    {

        // zax env
        define('WP_ENV', 'local_development');
        define('DB_NAME', 'igraonica');
        define('DB_USER', 'homestead');
        define('DB_PASSWORD', 'secret');
        define('DB_HOST', 'localhost');
        define('DB_CHARSET', 'utf8mb4');
        define('DB_COLLATE', '');
        define('DOMAIN_CURRENT_SITE', 'http://igraonica.zax.lan');
        define('WP_DEBUG', true);
        define( 'ALLOW_UNFILTERED_UPLOADS', true );
    }
}

define('WP_HOME', DOMAIN_CURRENT_SITE);
define('WP_SITEURL', DOMAIN_CURRENT_SITE);

define('AUTH_KEY',         '@f]~y4qH[;h+2 [*rT#~GM, 8eDxEg2f-L3Z8X_Qd[86IW.H&dML_Fn6lya]ARre');
define('SECURE_AUTH_KEY',  ',uC>li_.P%2,P^ZI~ltfW^2j.QKyr@|;)q+k,:aLF_t&ycvbbnj_dIBK5QIRv*fy');
define('LOGGED_IN_KEY',    'fE>9POZwE:n>%`Qrvv# FUlrTcw)6m#FVmT-.zf@jc&CtA|:fyc%iyY%HpQr#vA,');
define('NONCE_KEY',        'O w>WP>$)$2jY<k:by4mUT2hYqaH?vD)tSxDGz~s<##s^!Kj^[x2vlz$Ly7*ewk|');
define('AUTH_SALT',        '[3IZX,9H8/&p@Gz0-LOR+C?,|7{w@A(N7b,_VXt@(N-840,TutD%^^_DhkwIwa2*');
define('SECURE_AUTH_SALT', 'Shs~OTF.*Gq`b[[PD-z/OC[oy(ZeJa`].U78D]nqoROf9-+~s 8s:a(kKx^i#KFT');
define('LOGGED_IN_SALT',   'Em9CnyzotK5*kan*V6lo*ZKzLBYG^n&9{LU/IAKcFgp <D$0b$4c&m:/r/gO83:|');
define('NONCE_SALT',       '.W^-F@RacPFzf$>F#:aJ31%^Qlue+qHgm&f,C}dAX}vAA~vA_,EEE_<JIdA,c_Wn');

$table_prefix  = 'ic_';

if ( !defined('ABSPATH') ) define('ABSPATH', dirname(__FILE__) . '/');

require_once(ABSPATH . 'wp-settings.php');
