$(function() {

	var _cookie = document.cookie;
	var _consent_given = _cookie.search('cookieconsent=given');
	var _cookie_expiry = 120 * 24 * 60 * 60 * 1000;
	var _today = new Date();

	var _cookie_setings = {
		contentText: 'Naše stranice koriste tzv. <a href="/o-kolacicima/">kolačiće</a> kako bismo vam osigurali bolje korisničko iskustvo i funkcionalnost. Koristeći naše stranice slažete se s korištenjem kolačića.',
		acceptanceText: 'Prihvaćam',
		popupStyles: {
			'font-family': 'sans-serif',
			'font-size': '10px',
			'width': '280px',
			'position': 'fixed',
			'bottom': '10px',
			'right': '10px',
			'background-color': 'rgba(255,255,255,0.8)',
			'color': '#666',
			'padding': '5px 10px',
			'z-index': '999999'
		},
		buttonStyles: {
			'display': 'inline-block',
			'font-size': '12px',
			'padding': '5px 10px',
			'background-color': '#900',
			'color': 'white',
			'text-decoration': 'none'
		}
	}

	if(typeof _cookie_options !== 'undefined') $.extend(true, _cookie_setings, _cookie_options);

	var _cookie_popup = $('<div/>', { id: 'cookie-popup', class: 'popup-content' });

	if(_consent_given == -1){
		$('body').append(_cookie_popup);
		$('#cookie-popup').css(_cookie_setings.popupStyles).html('<p>' + _cookie_setings.contentText + '</p><p><a href="#" id="give-cookie-consent">' + _cookie_setings.acceptanceText + '</a></p>');
	}

	$('#give-cookie-consent').css(_cookie_setings.buttonStyles).on('click', function(e){
		e.preventDefault();
		_today.setTime(_today.getTime() + _cookie_expiry);
		document.cookie = 'cookieconsent=given; expires=' + _today.toUTCString() + '; path=/';
		$('#cookie-popup').hide();
	});
});