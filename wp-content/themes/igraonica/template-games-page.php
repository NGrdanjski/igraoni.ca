<?php
/*
 * Template Name: Igre
 */

$context = Timber::get_context();
$context['post'] = new Timber\Post();

Timber::render('templates/template-games-page.twig', $context);