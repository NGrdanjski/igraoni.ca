<?php
/*
 * Template Name: Timeline
 */

$context = Timber::get_context();
$context['post'] = new Timber\Post();

Timber::render('templates/template-timeline-page.twig', $context);