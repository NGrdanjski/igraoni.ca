export default class openMenu {

    constructor(element) {
        const container = document.querySelector('.js-ham');
        const menuContainer = document.querySelector('.menu--main');


        element.addEventListener('click', () => {
            console.log('Open menu!');
            this.showMenu(container, menuContainer)
        });
    }

    showMenu(container, menuContainer){
        container.classList.toggle('span--ham--open');
        menuContainer.classList.toggle('menu--main--open');
    }

}
