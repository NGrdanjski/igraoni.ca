import $ from "jquery";

export default class giraffe {
    constructor(){
        let giraffe = $('.span--giraffe');
        let giraffeOffset = giraffe.offset();
        let giraffeTop = giraffeOffset.top;
        let giraffeLeft = giraffeOffset.left;
        let speechBubble = $('.speech-bubble-wrapper');
        let speechBubbleWidth = $('.speech-bubble-wrapper').width();

        $(speechBubble).css('top', giraffeTop-speechBubbleWidth-80);
        $(speechBubble).css('left', giraffeLeft+speechBubbleWidth+80);
    }
}



