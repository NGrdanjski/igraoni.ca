export default class openInfo {

    constructor(element) {
        const container = document.querySelector('.js-info');
        const menuContainer = document.querySelector('.menu--info');


        element.addEventListener('click', () => {
            console.log('Open info menu!');
            this.showMenu(container, menuContainer)
        });
    }

    showMenu(container, menuContainer){
        container.classList.toggle('span--info--open');
        menuContainer.classList.toggle('menu--info--open');
    }

}
