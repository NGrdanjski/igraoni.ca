import $ from 'jquery';
import ScrollReveal from 'scrollreveal'
import 'jquery.nicescroll';
// Import ScrollMagic library
import ScrollMagic from 'scrollmagic';



import Hello from './components/helloWorld';
import ShowBox from './components/showBox';
import OpenMenu from './components/openMenu';
import OpenInfo from './components/openInfo';
import HomeParticles from './components/homeParticles';
import Giraffe from './components/giraffe';
import PageParticles from './components/pageParticles';

const components = [
    {
        class: Hello,
        selector: '.hello'
    },
    {
        class: ShowBox,
        selector: '.js-show-box'
    },
    {
        class: OpenMenu,
        selector: '.span--ham'
    },
    {
        class: OpenInfo,
        selector: '.span--info'
    },
    {
        class: HomeParticles,
        selector: '.home-particles'
    },
    {
        class: PageParticles,
        selector: '.page-particles'
    },
    {
        class: Giraffe,
        selector: '.span--giraffe'
    }
];

components.forEach(component =>{
    //console.log(component);
    if (document.querySelector(component.selector) !== null)
    {
        document.querySelectorAll(component.selector).forEach(
            element => new component.class(element, component.options)
        );
    }
});











let lastScrollTop = 0;
$('.scrolable').scroll(function(event){
    var st = $(this).scrollTop();
    if (st > lastScrollTop)
    {
        let p = st/10;
        $('.span--parallax').css("background-position-y", -p);
    }
    else
    {
        let p = st/10;
        $('.span--parallax').css("background-position-y", -p);
    }
    lastScrollTop = st;
});








let scrolableEl = $('.scrolable');
let boxHeroEl = $('.box--hero');
let w = window.innerWidth;
let h = window.innerHeight;
let pt = h/4.2;

$('.scrolable').css('height', h).css('padding-top', pt);


$(document).ready(function() {
    let header = $('.box--hero');
    let range = 150;

    $('.scrolable').on('scroll', function () {

        let scrollTop = $(this).scrollTop(),
            height = header.outerHeight(),
            offset = height / 1.3,
            calc = 1 - (scrollTop - offset + range) / range;

        //console.log(calc);

        header.css({ 'opacity': calc });

        if (calc > '1')
        {
            header.css({ 'opacity': 1 });
        }
        else if ( calc < '0' )
        {
            header.css({ 'opacity': 0 });
        }

    });

    $(".scrolable").niceScroll({
        speed: 50
    });

});


if ( $('body').hasClass('page-template-front-page') )
{
    console.log('naslovnicaaa');
}


ScrollReveal.debug = true;

ScrollReveal().reveal('.span--ham', {
    delay: 1400
});
ScrollReveal().reveal('.span--logo', {
    delay: 1550,
});
ScrollReveal().reveal('.span--info', {
    delay: 3500
});
ScrollReveal().reveal('.span--lion', {
    delay: 1900
});
ScrollReveal().reveal('.span--giraffe', {
    delay: 1900
});
ScrollReveal().reveal('#particles-js', {
    delay: 1800
});
ScrollReveal().reveal('.scrolable-w', {
    delay: 1600,
    scale: 0.30
});
ScrollReveal().reveal('.slider--games', {
    delay: 2200,
    scale: 0.30
});

ScrollReveal().reveal('.speech-bubble-wrapper', {
    delay: 3500,
    scale: 0.30
});
// ScrollReveal().reveal('.box--subhero', {
//     delay: 1800,
//     scale: 0.30,
//     distance: '100px',
//     origin: 'bottom'
// });
ScrollReveal().reveal('.span--down', {
    delay: 2400,
    scale: 0.30
});
ScrollReveal().reveal('.span--parallax', {
    delay: 800,
    scale: 2.0,
    opacity: 0.0
});
ScrollReveal().reveal('.section--main', {
    delay: 200,
});



if ( $('body').hasClass('page-template-template-timeline-page') )
{

    let counter = 0;
    $("body").keydown(function(e){

        if ( counter <= 0 )
        {
            // left
            if ( e.keyCode == 37 )
            {
                console.log('Movin left...');
                counter ++;
                console.log(counter);

                // repeat loop forever for tlo
                let tl = new TimelineMax({repeat:-1});

                // cloud left
                tl.to(".timemachine__cloud-left", 0,{
                    left: 410 + counter*5
                });

                // cloud right
                tl.to(".timemachine__cloud-right", 0,{
                    right: 165 - counter*4
                });

                // tree left
                tl.to(".timemachine__tree-left", 0,{
                    left: 30 + counter*3
                });
                tl.to(".timemachine__tree-left-stone", 0,{
                    left: -65 + counter*3
                });


                // tree center
                tl.to(".timemachine__tree-center", 0,{
                    right: `calc(50% + ${Math.abs(counter*3)}px)`
                });
                // tree center stone
                tl.to(".timemachine__tree-center-stone", 0,{
                    right: `calc(50% + 70px + ${Math.abs(counter*3)}px)`
                });

                // bush left
                tl.to(".timemachine__bush-left", 0,{
                    left: 150 + counter*2
                });

                // bush right
                tl.to(".timemachine__bush-right", 0,{
                    right: -360 - counter*2
                });


                tl.to(".timemachine__drvo01", 0,{
                    right: -90 - counter*3
                });
                tl.to(".timemachine__drvo01-kamen", 0,{
                    right: 85 - counter*3
                });


                tl.to(".timemachine__drvo02", 0,{
                    right: 195 - counter*3
                });
                tl.to(".timemachine__drvo02-kamen", 0,{
                    right: 425 - counter*3
                });


                tl.to(".panda", 0,{
                    rotation: -counter*3
                });
                tl.to(".current-year", 0,{
                    left: -counter*3 + 'px'
                });
                tl.to(".timemachine__tlo", 0,{
                    backgroundPosition: counter*3 + 'px'
                });
                tl.to(".timemachine__planine03", 0,{
                    backgroundPosition: counter*2 + 'px'
                });
                tl.to(".timemachine__planine02", 0,{
                    backgroundPosition: counter + 'px'
                });
                tl.to(".timemachine__planine01", 0,{
                    backgroundPosition: counter/2 + 'px'
                });
                return false;
            }

            // right
            else  if ( e.keyCode == 39 )
            {
                console.log('Movin right...');
                counter --;
                console.log(counter);

                $('.pandaa').addClass('hoda');

                // repeat loop forever for tlo
                let tl = new TimelineMax({repeat:-1});

                // cloud left
                tl.to(".timemachine__cloud-left", 0,{
                    left: 410 + counter*5
                });

                // cloud right
                tl.to(".timemachine__cloud-right", 0,{
                    right: 165 - counter*4
                });

                // tree left
                tl.to(".timemachine__tree-left", 0,{
                    left: 30 + counter*3
                });
                tl.to(".timemachine__tree-left-stone", 0,{
                    left: -65 + counter*3
                });

                // tree center
                tl.to(".timemachine__tree-center", 0,{
                    right: `calc(50% + ${Math.abs(counter*3)}px)`
                });
                // tree center stone
                tl.to(".timemachine__tree-center-stone", 0,{
                    right: `calc(50% + 70px + ${Math.abs(counter*3)}px)`
                });

                // bush left
                tl.to(".timemachine__bush-left", 0,{
                    left: 150 + counter*2
                });

                // bush right
                tl.to(".timemachine__bush-right", 0,{
                    right: -360 - counter*2
                });


                tl.to(".timemachine__drvo01", 0,{
                    right: -90 -counter*3
                });
                tl.to(".timemachine__drvo01-kamen", 0,{
                    right: 85 -counter*3
                });


                tl.to(".timemachine__drvo02", 0,{
                    right: 195 - counter*3
                });
                tl.to(".timemachine__drvo02-kamen", 0,{
                    right: 425 - counter*3
                });


                tl.to(".panda", 0,{
                    rotation: -counter*3
                });
                tl.to(".timemachine__tlo", 0,{
                    backgroundPosition: counter*3 + 'px'
                });
                tl.to(".current-year", 0,{
                    left: -counter*3 + 'px'
                });
                tl.to(".timemachine__planine03", 0,{
                    backgroundPosition: counter*2 + 'px'
                });
                tl.to(".timemachine__planine02", 0,{
                    backgroundPosition: counter + 'px'
                });
                tl.to(".timemachine__planine01", 0,{
                    backgroundPosition: counter/2 + 'px'
                });
                return false;
            }

            // Up
            else  if (e.keyCode == 38)
            {
                console.log('Up');
                return false;
            }
            // Down
            else  if (e.keyCode == 40)
            {
                console.log('Down');
                return false;
            }
            else
            {
                $('.pandaa').removeClass('hoda');
            }
        }

    });
}
