<?php

/**
 * Registered Custom Post Types
 * @author NGrdanjski
 */
function registerCustomPostsTypes() {

	/**
	 * Post Type: Filmovi.
	 */

//	$labels = array(
//		"name" => __( "Filmovi", "" ),
//		"singular_name" => __( "Film", "" ),
//        'add_new_item'          => __( 'Dodaj Novi Film', 'text_domain' ),
//        'add_new'               => __( 'Dodaj Novi Film', 'text_domain' ),
//        'new_item'              => __( 'Novi Film', 'text_domain' ),
//        'edit_item'             => __( 'Uredi Film', 'text_domain' ),
//        'update_item'           => __( 'Update Item', 'text_domain' ),
//        'view_item'             => __( 'Pogledaj Film', 'text_domain' ),
//        'view_items'            => __( 'Pogledaj Filmove', 'text_domain' ),
//        'search_items'          => __( 'Traži Film', 'text_domain' ),
//	);
//
//	$args = array(
//		"label" => __( "Filmovi", "" ),
//		"labels" => $labels,
//		"description" => "",
//		"public" => true,
//		"publicly_queryable" => true,
//		"show_ui" => true,
//		"show_in_rest" => false,
//		"rest_base" => "",
//		"has_archive" => true,
//		"show_in_menu" => true,
//		"exclude_from_search" => false,
//		"capability_type" => "post",
//		"map_meta_cap" => true,
//		"hierarchical" => true,
//		"rewrite" => array(
//			"slug" => "film",
//			"with_front" => true
//		),
//		"query_var" => true,
//		"supports" => array(
//			"title",
//			"editor",
//			"thumbnail",
//			"page-attributes"
//		),
//        "menu_icon" => "dashicons-video-alt2",
//	);
//
//	register_post_type( "filmovi", $args );


}

add_action( 'init', 'registerCustomPostsTypes' );



/**
 * Registered Custom Post Taxonomy
 * @author NGrdanjski
 */
function registerCustomTaxonomyTypes() {

	/**
	 * Taxonomy: Tip.
	 */

//	$labels = array(
//		"name" => __( "Tipovi Filmova", "" ),
//		"singular_name" => __( "Tip", "" ),
//	);
//
//	$args = array(
//		"label" => __( "Tip", "" ),
//		"labels" => $labels,
//		"public" => true,
//		"hierarchical" => true,
//		"label" => "Tip",
//		"show_ui" => true,
//		"show_in_menu" => true,
//		"show_in_nav_menus" => true,
//		"query_var" => true,
//		"rewrite" => array( 'slug' => 'tip', 'with_front' => true, ),
//		"show_admin_column" => true,
//		"show_in_rest" => false,
//		"rest_base" => "",
//		"show_in_quick_edit" => false,
//	);
//	register_taxonomy( "Tip", array( "filmovi" ), $args );
}

add_action( 'init', 'registerCustomTaxonomyTypes' );