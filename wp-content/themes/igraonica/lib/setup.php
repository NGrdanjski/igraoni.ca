<?php

namespace Drap\Theme\Setup;

use Drap\Theme\Assets;

/**
 * Theme setup
 */
function setup() {
  // Enable features from Soil when plugin is activated
  // https://roots.io/plugins/soil/
  add_theme_support('__oxigen-clean-up');
  add_theme_support('__oxigen-nav-walker');
  add_theme_support('__oxigen-nice-search');
  //add_theme_support('__oxigen-jquery-cdn');
  add_theme_support('__oxigen-relative-urls');

  // Make theme available for translation
  // Community translations can be found at https://github.com/roots/sage-translations
  load_theme_textdomain('theme', get_template_directory() . '/lang');

  // Enable plugins to manage the document title
  // http://codex.wordpress.org/Function_Reference/add_theme_support#Title_Tag
  add_theme_support('title-tag');

  // Register wp_nav_menu() menus
  // http://codex.wordpress.org/Function_Reference/register_nav_menus
  register_nav_menus([
    'primary_navigation' => __('Primary Navigation', 'theme'),
    'footer_navigation' => __('Footer Navigation', 'theme')
  ]);

  // Enable post thumbnails
  // http://codex.wordpress.org/Post_Thumbnails
  // http://codex.wordpress.org/Function_Reference/set_post_thumbnail_size
  // http://codex.wordpress.org/Function_Reference/add_image_size
  add_theme_support('post-thumbnails');

  // Enable post formats
  // http://codex.wordpress.org/Post_Formats
  //add_theme_support('post-formats', ['aside', 'gallery', 'link', 'image', 'quote', 'video', 'audio']);

  // Enable HTML5 markup support
  // http://codex.wordpress.org/Function_Reference/add_theme_support#HTML5
  add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);

  // Use main stylesheet for visual editor
  // To add custom styles edit /assets/styles/layouts/_tinymce.scss
  add_editor_style(Assets\asset_path('styles/main.css'));
}
add_action('after_setup_theme', __NAMESPACE__ . '\\setup');

/**
 * Theme assets
 */
function assets() {

    // css
    wp_enqueue_style('app-css', Assets\asset_path('app.css'), false, 11);

    // comment scripts --> show this file only on the single page
    if (is_single() && comments_open() && get_option('thread_comments'))
    {
        wp_enqueue_script('comment-reply');
    }

    // head js
    //wp_enqueue_script('app-head', Assets\asset_path('scripts/app-head.js'), null, false);

    // footer js
    wp_enqueue_script('app-footer', Assets\asset_path('app.js'), null, false, true);

}
add_action('wp_enqueue_scripts', __NAMESPACE__ . '\\assets', 100);
