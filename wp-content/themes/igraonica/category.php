<?php

$context = Timber::get_context();
$context['post'] = new Timber\Post();
$context['category'] = new TimberTerm($params['post_term']);
$context['page_title'] = $params['post_child_term'];



$posts_tax_query = array(
    'post_type' => 'post',
    'tax_query' => array(
        array(
            'taxonomy' => 'category',
            'field'    => 'slug',
            'terms'    => $params['post_child_term'],
        )
    ),
    'posts_per_page' => 10
);
$context['posts'] = Timber::get_posts($posts_tax_query);

Timber::render('templates/category.twig', $context);

