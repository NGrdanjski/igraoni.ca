<?php
/*
 * Template Name: Životinjsko carstvo
 */

$context = Timber::get_context();
$context['post'] = new Timber\Post();

Timber::render('templates/template-animal-kingdom-page.twig', $context);