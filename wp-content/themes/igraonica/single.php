<?php
$context = Timber::get_context();
$context['post'] = new Timber\Post();

$context['category'] = new TimberTerm($params['post_term']);
//print_r($params);

//$inputFileName = get_field('exel_file');


function getExcellCallToTheWorld($cached = true)
{
    $key = "excell_call_to_the_world";


    $cachedData = wp_cache_get( $key, 'smart' );
    if($cachedData == false)
    {

        $excell_call_to_the_world = get_field('exel_file');

        if($excell_call_to_the_world)
        {
            $urlStr = $excell_call_to_the_world['url'];
            if( strpos($excell_call_to_the_world['url'], "/media/") === 0 )
            {
                $urlStr = str_replace('/media/', '/wp-content/uploads/', $excell_call_to_the_world['url']);
            }

            $urlArr = parse_url($urlStr);

            $xlsx_file = rtrim(ABSPATH, '/') .  $urlArr['path'];
            if(file_exists($xlsx_file))
            {
                $cachedData = array();
                $cachedData['columns'] = array();
                $cachedData['zones'] = array();
                $cachedData['codes'] = array();

                require(get_template_directory() . '/lib/XLSXReader.php');
                $xlsx = new XLSXReader($xlsx_file);
                $sheets = $xlsx->getSheetNames();

                $sheet = false;
                if(!empty($sheets))
                {
                    $sheet = current($sheets);
                }

                if($sheet)
                {

                    $data = $xlsx->getSheetData($sheet);
                    if(!empty($data))
                    {
                        foreach($data as $key=>$row)
                        {
                            if($key==0)
                            {
                                $cachedData['columns'] = $row;
                            }
                            else
                            {
                                if(empty($cachedData['zones'][$row[3]]))
                                {
                                    /*
                                    Zone 1 = 4 cents/min
                                    Zone 2 = 4 cents/min
                                    Zone 3 = 13 cents/min
                                    Zone 4 = 20 cents/min
                                    Zone 5 = 35 cents/min
                                    Zone 6 = 50 cents/min
                                    Zone 7 = $1/min
                                    Zone 8 = $7/min
                                    */
                                    $price = "";

                                    if($row[4]<1)
                                    {
                                        $price = intval($row[4] * 100) . " cents";
                                    }
                                    else if($row[4]>=1)
                                    {
                                        $price = "$" . ($row[4]) . "";
                                    }

                                    $cachedData['zones'][$row[3]] = array(
                                        "name" => $row[3],
                                        "price" => $price,
                                        "value" => $row[4],
                                    );
                                }

                                $cachedData['codes'][$row[3]][] = $row;
                            }
                        }

                        wp_cache_add( $key, $cachedData, 'smart',  100000);
                    }
                }
            }
        }
    }

    return $cachedData;
}


$context['service_call_to_the_world_block'] = getExcellCallToTheWorld();

//print_r($context['service_call_to_the_world_block']);
//die();








//global $post;
//$visitor_count = get_post_meta( $post->ID, '_post_views_count', true);
//if( $visitor_count == '' ){ $visitor_count = 0; }
//if( $visitor_count >= 1000 ){
//    $visitor_count = round( ($visitor_count/1000), 2 );
//    $visitor_count = $visitor_count.'k';
//}
//echo esc_attr($visitor_count);

//print_r($context['post']->rucni_odabir);

if(!$context['post']->rucni_odabir)
{

    $query_args = array(
        'post_type' => 'post',
        'post_status' => 'publish',
        'tax_query' => array(
            array(
                'taxonomy' => 'category',
                'field' => 'slug',
                'terms' => $params['post_term'],
            ),
        ),
        'posts_per_page' => 4,
    );
    $context['related_posts'] = Timber::get_posts($query_args);
}



//
//
// TOP POSTS SIDEBAR
//
//
include_once 'sidebar.php';



Timber::render('templates/single.twig', $context);