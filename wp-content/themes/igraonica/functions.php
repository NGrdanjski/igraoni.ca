<?php

$theme_includes = [
	'lib/timber.php',            // Twig magic
	'lib/assets.php',    // Scripts and stylesheets
	'lib/extras.php',    // Custom functions
	'lib/setup.php',     // Theme setup
	'lib/titles.php',    // Page titles
	'lib/customizer.php', // Theme customizer
	'lib/acf.php',
	'lib/cpt.php'
];

foreach ($theme_includes as $file)
{
	if (!$filepath = locate_template($file))
	{
		trigger_error(sprintf(__('Error locating %s for inclusion', 'theme'), $file), E_USER_ERROR);
	}

	require_once $filepath;
}
unset($file, $filepath);


/**
 * Enable SVG
 * @author Nikola Grdanjski
 */
function cc_mime_types($mimes) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');



/**
 * Enqueue Admin-ajax Script
 * @author Nikola Grdanjski
 */
function ajaxScripts() {
	// enqueue script
	wp_register_script('afp_script', '', false, null, false);
	wp_localize_script( 'afp_script', 'afp_vars', array(
			'afp_nonce' => wp_create_nonce( 'afp_nonce' ),
			'afp_ajax_url' => admin_url( 'admin-ajax.php' ),
		)
	);
	wp_enqueue_script('afp_script');
}
add_action('wp_enqueue_scripts', 'ajaxScripts', 100);





//
//
//
//
//
function timber_twig($twig)
{
	$twig->addExtension(new Twig_Extension_StringLoader());
	$twig->addFilter(new Twig_SimpleFilter('print_r', 'smart_print_r'));

	return $twig;
}
add_filter('timber/twig', 'timber_twig');

function smart_print_r($row)
{
	print_r($row);
}



//
//
// Remove WordPress menu from admin bar
//
//
add_action( 'admin_bar_menu', 'remove_wp_logo', 999 );
function remove_wp_logo( $wp_admin_bar ) {
    $wp_admin_bar->remove_node( 'wp-logo' );
}

//
//
// Admin footer modification
//
//
function remove_footer_admin ()
{
    echo '';
}

add_filter('admin_footer_text', 'remove_footer_admin');





//
//
//
//
/**
 * Assign term to parent terms
 * @author NIVAS | Nikola Grdanjski
 */
//add_action('save_post', 'assign_parent_terms', 10, 2);
function assign_parent_terms($post_id, $post){

    if( $post->post_type != 'post' )
    {
        return $post_id;
    }

    // get all assigned terms
    $terms = wp_get_post_terms($post_id, 'category' );
    foreach( $terms as $term )
    {
        while($term->parent != 0 && !has_term( $term->parent, 'category', $post ))
        {
            // move upward until we get to 0 level terms
            wp_set_post_terms($post_id, array($term->parent), 'category', true);
            $term = get_term($term->parent, 'category');
        }
    }
}


//
//
//
//
//
function ajax_load_more_devices() {

    // Verify nonce
    if( !isset( $_GET['afp_nonce'] ) || !wp_verify_nonce( $_GET['afp_nonce'], 'afp_nonce' ) )
    {
        die('Permission denied');
    }

    //exit('aaa');

    $res = array(
        'success' => true,
        'html' => '',
        'page' => 1,
        'brand' => '',
        'count' => 0,
        'load_more' => 1,
        'query' => array()
    );

    $brand = !empty($_GET["brand"]) && $_GET["brand"]!=='all' ? trim($_GET["brand"]) : 'devices';
    $page = !empty($_GET["page"]) ? trim($_GET["page"]) : 'page';

    $res['page'] = $page;
    $res['brand'] = $brand;

    //print_r($res);
    //die();





    ob_start();
    Timber::render('templates/ajax-content.twig', $context);
    $res['html'] = ob_get_contents();
    ob_end_clean();

    //print_r($res['html']);
    header('Content-Type: application/json');
    exit(json_encode($res));
}
add_action('wp_ajax_devices_load_more', 'ajax_load_more_devices');
add_action('wp_ajax_nopriv_devices_load_more', 'ajax_load_more_devices');



//
//
// AJAX NAJPOPULARNIJI TJEDNO
//
//
function ajax_tjedni_top() {

    //
    if( !isset( $_GET['afp_nonce'] ) || !wp_verify_nonce( $_GET['afp_nonce'], 'afp_nonce' ) )
    {
        die('Permission denied');
    }

    $res = array(
        'success' => true,
        'html' => '',
        'query' => array()
    );

    $today = date('F jS, Y');
    $minus_7_days = date('F jS, Y', strtotime("-1 week"));

    $query_args = array(
        'post_type' => 'post',
        'date_query' => array(
            array(
                'after' => $minus_7_days,
                'before' => $today,
            ),
        ),
        'meta_key' => '_post_views_count',
        'orderby' => 'meta_value_num',
        'order' => 'DESC',
        'posts_per_page' => 5,
    );
    $context['tjedi_top'] = Timber::get_posts($query_args);

    //
    ob_start();
    Timber::render('templates/ajax-top-posts.twig', $context);
    $res['html'] = ob_get_contents();
    ob_end_clean();

    header('Content-Type: application/json');
    exit(json_encode($res));
}
add_action('wp_ajax_ajax_tjedni_top', 'ajax_tjedni_top');
add_action('wp_ajax_nopriv_ajax_tjedni_top', 'ajax_tjedni_top');



//
//
// AJAX NAJPOPULARNIJI MJESECNO
//
//
function ajax_najpopularniji_mjesecno() {

    //
    if( !isset( $_GET['afp_nonce'] ) || !wp_verify_nonce( $_GET['afp_nonce'], 'afp_nonce' ) )
    {
        die('Permission denied');
    }

    $res = array(
        'success' => true,
        'html' => '',
        'query' => array()
    );

    $today = date('F jS, Y');
    $minus_1_month = date('F jS, Y', strtotime("- 1 month"));

    $query_args = array(
        'post_type' => 'post',
        'date_query' => array(
            array(
                'after' => $minus_1_month,
                'before' => $today,
            ),
        ),
        'meta_key' => '_post_views_count',
        'orderby' => 'meta_value_num',
        'order' => 'ASC',
        'posts_per_page' => 5,
    );
    $context['tjedi_top'] = Timber::get_posts($query_args);

    //
    ob_start();
    Timber::render('templates/ajax-top-posts.twig', $context);
    $res['html'] = ob_get_contents();
    ob_end_clean();

    header('Content-Type: application/json');
    exit(json_encode($res));
}
add_action('wp_ajax_ajax_najpopularniji_mjesecno', 'ajax_najpopularniji_mjesecno');
add_action('wp_ajax_nopriv_ajax_najpopularniji_mjesecno', 'ajax_najpopularniji_mjesecno');



//
//
// AJAX NAJPOPULARNIJI GODISNJE
//
//
function ajax_najpopularniji_godisnje() {

    //
    if( !isset( $_GET['afp_nonce'] ) || !wp_verify_nonce( $_GET['afp_nonce'], 'afp_nonce' ) )
    {
        die('Permission denied');
    }

    $res = array(
        'success' => true,
        'html' => '',
        'query' => array()
    );

    $today = date('F jS, Y');
    $minus_1_year = date('F jS, Y', strtotime("- 1 year"));

    $query_args = array(
        'post_type' => 'post',
        'date_query' => array(
            array(
                'after' => $minus_1_year,
                'before' => $today,
            ),
        ),
        'meta_key' => '_post_views_count',
        'orderby' => 'meta_value_num',
        'order' => 'ASC',
        'posts_per_page' => 5,
    );
    $context['tjedi_top'] = Timber::get_posts($query_args);

    //
    ob_start();
    Timber::render('templates/ajax-top-posts.twig', $context);
    $res['html'] = ob_get_contents();
    ob_end_clean();

    header('Content-Type: application/json');
    exit(json_encode($res));
}
add_action('wp_ajax_ajax_najpopularniji_godisnje', 'ajax_najpopularniji_godisnje');
add_action('wp_ajax_nopriv_ajax_najpopularniji_godisnje', 'ajax_najpopularniji_godisnje');




//
//
//
//
//
function ajax_load_more_news() {

    // Verify nonce
    if( !isset( $_GET['afp_nonce'] ) || !wp_verify_nonce( $_GET['afp_nonce'], 'afp_nonce' ) )
    {
        die('Permission denied');
    }

    //exit('aaa');

    $res = array(
        'success' => true,
        'html' => '',
        'page' => 1,
        'brand' => '',
        'count' => 0,
        'load_more' => 1,
        'query' => array()
    );

    $tax_term = !empty($_GET["tax_term"]) ? trim($_GET["tax_term"]) : 'tax_term';


    $res['page'] = $tax_term;

    if ($res['page'] == 'sve')
    {
        $news_term_query = array(
            'post_type' => 'post',
            'post_status' => 'publish',
            'tax_query' => array(
                array(
                    'taxonomy' => 'category',
                    'field' => 'slug',
                    'terms' => 'novosti',
                ),
            ),
            'posts_per_page' => 6
        );
        $context['news_term_posts'] = Timber::get_posts($news_term_query);
    }
    else
    {
        $news_term_query = array(
            'post_type' => 'post',
            'post_status' => 'publish',
            'tax_query' => array(
                array(
                    'taxonomy' => 'category',
                    'field' => 'slug',
                    'terms' => $res['page'],
                ),
            ),
            'posts_per_page' => 6
        );
        $context['news_term_posts'] = Timber::get_posts($news_term_query);
    }




    ob_start();
    Timber::render('templates/ajax-content2.twig', $context);
    $res['html'] = ob_get_contents();
    ob_end_clean();

    //print_r($res['html']);
    header('Content-Type: application/json');
    exit(json_encode($res));
}
add_action('wp_ajax_ajax_load_more_news', 'ajax_load_more_news');
add_action('wp_ajax_nopriv_ajax_load_more_news', 'ajax_load_more_news');


//
function ryanbenhase_unregister_tags() {
    unregister_taxonomy_for_object_type( 'post_tag', 'post' );
}
add_action( 'init', 'ryanbenhase_unregister_tags' );



//
//
//
//
//
//Routes::map('/:post_term/:post_child_term', function( $params ){
//
//    Routes::load('category.php', $params, null, 200);
//
//});
//
//Routes::map('/:post_term/:post_child_term/:post_slug', function( $params ){
//
//    Routes::load('single.php', $params, null, 200);
//
//});


//
//
//
//
//
function rm_post_view_count(){
    if (is_single())
    {
        global $post;

        $count_post = esc_attr( get_post_meta( $post->ID, '_post_views_count', true) );

        if( $count_post == '')
        {
            $count_post = 1;
            add_post_meta( $post->ID, '_post_views_count', $count_post);
        }
        else
        {
            $count_post = (int)$count_post + 1;
            update_post_meta( $post->ID, '_post_views_count', $count_post);
        }
    }
}
add_action('wp_head', 'rm_post_view_count');



//
//
//
//
//
function add_new_posts_admin_column($column) {
    $column['post_views'] = 'Views';
    return $column;
}
add_filter('manage_posts_columns', 'add_new_posts_admin_column');

//
function add_new_posts_admin_column_show_value($column_name) {
    if ($column_name == 'post_views')
    {
        echo get_field('_post_views_count');
    }
}
add_action('manage_posts_custom_column', 'add_new_posts_admin_column_show_value', 10, 2);