<?php
/*
 * Template Name: Naslovnica
 */

$context = Timber::get_context();
$context['post'] = new Timber\Post();

Timber::render('templates/front-page.twig', $context);