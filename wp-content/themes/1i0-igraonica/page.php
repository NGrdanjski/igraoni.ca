<?php
/**
 * Single page template file
 *
 * @package  WordPress
 * @subpackage  SageTimber
 * @since  SageTimber 0.1
 */

$context = Timber::get_context();
$context['post'] = new Timber\Post();

$params = TimberUrlHelper::get_params();
$cuttentCategoty = $params[0];

$posts_from_category = array(
    'post_type'         => 'post',
    'category_name' => $cuttentCategoty,
    'posts_per_page'    => '20',
);
$context['category_posts'] = Timber::get_posts($posts_from_category);



// Latest news
$latest_posts = array(
    'post_type'         => 'post',
    'posts_per_page'    => '6',
);
$context['latest_posts'] = Timber::get_posts($latest_posts);

// random news
$random_posts = array(
    'post_type'         => 'post',
    'orderby' => 'rand',
    'posts_per_page'    => '7',
);
$context['rand_posts'] = Timber::get_posts($random_posts);

// izdvojeno news
$izdvojeno_posts = array(
    'post_type'         => 'post',
    'orderby' => 'rand',
    'posts_per_page'    => '7',
);
$context['izdvojeno_posts'] = Timber::get_posts($izdvojeno_posts);


Timber::render('pages/page.twig', $context);