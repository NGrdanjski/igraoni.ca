<?php

use Ams\Includes as Includes;


/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
    'lib/timber.php',
    'lib/assets.php',
    'lib/extras.php',
    'lib/setup.php',
    'lib/titles.php',
    'lib/customizer.php',
    'lib/acf.php',
    'lib/cpt.php'
];

foreach ($sage_includes as $file)
{
  if (!$filepath = locate_template($file))
  {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);



add_filter('timber/twig', 'timber_twig');

function timber_twig($twig)
{
    $twig->addExtension(new Twig_Extension_StringLoader());
    $twig->addFilter(new Twig_SimpleFilter('print_r', 'smart_print_r'));

    return $twig;
}

function smart_print_r($row)
{
    print_r($row);
}



/**
 * AMS WP Options Page
 * @author => Nikola Grdanjski
 */
if( function_exists('acf_add_options_page') ) {
    acf_add_options_page(array(
        'page_title' 	=> 'Igraonica Settings',
        'menu_title'	=> 'Igraonica Settings',
        'menu_slug' 	=> 'igraonica-settings',
        'capability'	=> 'edit_posts',
//        'icon_url'      => 'dashicons-images-alt2',
//        'position'      => 100,
        'redirect'		=> false
    ));
}


// ACF Options fields
add_filter( 'timber_context', 'amsTimberContext'  );
function amsTimberContext( $context ) {
    $context['options'] = get_fields('option');
    return $context;
}



// Register admin css
function admin_style() {
    wp_enqueue_style('admin-styles', get_template_directory_uri().'/admin/admin.css');
}
add_action('admin_enqueue_scripts', 'admin_style');


// Remove WP logo from admin bar
function exampleAdminBarRemoveLogo() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu( 'wp-logo' );
}
add_action( 'wp_before_admin_bar_render', 'exampleAdminBarRemoveLogo', 0 );



//
//
// Images size
//add_image_size('340x404', 340, 404, true);
//add_image_size('372x484', 372, 484, true);
//add_image_size('600x321', 600, 321, true);
//add_image_size('736x404', 736, 404, true);
//add_image_size('768x512', 768, 512, true);
//add_image_size('875x560', 875, 560, true);
//add_image_size('1024x683', 1024, 683, true);
//add_image_size('1199x799', 1199, 799, true);
//add_image_size('1880x1100', 1880, 1100, true);

// Small Theme 2x940 || 2x550



/**
 * Enqueue Admin-ajax Script
 * @author Nikola Grdanjski
 */
function ajaxScripts() {
    // enqueue script
    wp_register_script('afp_script', '', false, null, false);
    wp_localize_script( 'afp_script', 'afp_vars', array(
            'afp_nonce' => wp_create_nonce( 'afp_nonce' ),
            'afp_ajax_url' => admin_url( 'admin-ajax.php' ),
        )
    );
    wp_enqueue_script('afp_script');
}
add_action('wp_enqueue_scripts', 'ajaxScripts', 100);



//
//
// AJAX IGRE
//
//
function ajax_games_game_one() {

    //
    if( !isset( $_GET['afp_nonce'] ) || !wp_verify_nonce( $_GET['afp_nonce'], 'afp_nonce' ) )
    {
        die('Permission denied');
    }

    $res = array(
        'success' => true,
        'html' => ''
    );

    ob_start();
    Timber::render('games/game-one.twig');
    $res['html'] = ob_get_contents();
    ob_end_clean();

    header('Content-Type: application/json');
    exit(json_encode($res));
}
add_action('wp_ajax_ajax_games_game_one', 'ajax_games_game_one');
add_action('wp_ajax_nopriv_games_game_one', 'ajax_games_game_one');


/**
 * Router
 * @author => Nikola Grdanjski
 */
Routes::map(__('igraj/slagalica', 'igraoni.ca'), function($params){
	Routes::load('game-image-puzzles.php');
});
// Bejeweled game route
Routes::map(__('igraj/spajalica', 'igraoni.ca'), function($params){
	Routes::load('game-bejeweled.php');
});
// Memory game route
Routes::map(__('igraj/pamtilica', 'igraoni.ca'), function($params){
    Routes::load('game-memory.php');
});