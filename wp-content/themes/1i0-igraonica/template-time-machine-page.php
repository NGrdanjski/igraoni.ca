<?php
/*
 * Template Name: Time Machine
 */

$context = Timber::get_context();
$context['post'] = new Timber\Post();

function bodyClass($classes) {
	$classes[] = 'time-machine-page';
	return $classes;
}
add_filter('body_class', 'bodyClass');

Timber::render('templates/template-time-machine-page.twig', $context);