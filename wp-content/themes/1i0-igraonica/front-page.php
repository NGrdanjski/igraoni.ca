<?php
/*
 * Template Name: Homepage
 */

$context = Timber::get_context();
$context['post'] = new Timber\Post();

Timber::render('templates/front-page.twig', $context);