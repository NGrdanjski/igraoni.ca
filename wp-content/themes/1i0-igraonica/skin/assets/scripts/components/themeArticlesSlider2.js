import $ from 'jquery';
import Swiper from 'swiper';

export default class themeArticlesSlider2 {

    constructor(element){
        const elementClass = '.' + element.getAttribute('class');
        let themeArticlesSlider2 = new Swiper ('.swiper-container-s', {
            direction: 'horizontal',
            loop: true,
            wrapperClass: 'swiper-wrapper-s',
            slidesPerView: 4,
            spaceBetween: 30,
            pagination: {
                el: '.swiper-pagination2',
                type: 'bullets',
                clickable: true
            },
            autoplay: {
                delay: 4000,
            },
            breakpoints: {
                // when window width is  <= 320px
                375: {
                    slidesPerView: 1,
                    pagination: {
                        el: '.swiper-pagination2',
                        type: 'bullets',
                        clickable: true
                    },
                },
                // when window width is <= 480px
                480: {
                },
                // when window width is <= 640px
                640: {
                    slidesPerView: 1,
                }
            }
        });
    }

}