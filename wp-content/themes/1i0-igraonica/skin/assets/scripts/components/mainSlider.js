import $ from 'jquery';
import Swiper from 'swiper';

export default class mainSlider {

    constructor(element){
        var mainSlider = new Swiper ('.main-slider__main-slides', {
            direction: 'horizontal',
            loop: true,
            wrapperClass: 's-wrapper',
            pagination: {
                el: '.swiper-pagination-1',
                clickable: true,
                renderBullet: function (index, className) {
                    let menu = [];
                    $(".s-wrapper .swiper-slide").each(function(i) {
                        menu.push($(this).data("name"));
                    });
                    return '<span class="' + className + '">' + (menu[index +1]) + '</span>';
                }
            },
            autoplay: {
                delay: 5000,
            },
            breakpoints: {
                // when window width is  <= 320px
                375: {
                    slidesPerView: 'auto',
                    centeredSlides: true,
                    spaceBetween: 10,
                    grabCursor: true,
                },
                // when window width is <= 480px
                480: {
                },
                // when window width is <= 640px
                640: {
                    slidesPerView: 1,
                }
            }
        });



        const slider = document.querySelector('.swiper-pagination-1');
        let isDown = false;
        let startX;
        let scrollLeft;
        //console.log(slider);

        slider.addEventListener('mousedown', (e) => {
            isDown = true;
            slider.classList.add('active');
            startX = e.pageX - slider.offsetLeft;
            scrollLeft = slider.scrollLeft;
        });
        slider.addEventListener('mouseleave', () => {
            isDown = false;
            slider.classList.remove('active');
        });
        slider.addEventListener('mouseup', () => {
            isDown = false;
            slider.classList.remove('active');
        });
        slider.addEventListener('mousemove', (e) => {
            if(!isDown) return;
            e.preventDefault();
            const x = e.pageX - slider.offsetLeft;
            const walk = (x - startX) * 3; //scroll-fast
            slider.scrollLeft = scrollLeft - walk;
        });

    }
}