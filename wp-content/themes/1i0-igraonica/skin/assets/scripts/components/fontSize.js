export default class fontSize {

    constructor(element) {
        //console.log(element);
        const decreaseEl = document.querySelector('.font-swicher__minus');
        const increaseEl = document.querySelector('.font-swicher__plus');

        let title = document.querySelector('.single-post .content-wrapper--single .wrapper__header .title--post');
        let subtitle = document.querySelector('.single-post .content-wrapper--single .wrapper__header .subtitle--post');
        let contentP = document.querySelector('.single-post .content-wrapper--single .wrapper__content p');
        let contentH1 = document.querySelector('.single-post .content-wrapper--single .wrapper__content h1');
        let contentH2 = document.querySelector('.single-post .content-wrapper--single .wrapper__content h2');
        let contentH3 = document.querySelector('.single-post .content-wrapper--single .wrapper__content h3');
        //console.log(title);

        let i = 1;

        // decrease event
        decreaseEl.addEventListener('click', () => {
            this.decrease(title, subtitle, i)
        });

        // increase event
        increaseEl.addEventListener('click', () => {
            this.increase()
        });
    }

    decrease(title, subtitle, i){
        console.log('Decrease!');
        i++;
        console.log(i);
        title.style.fontSize = '.7em';
        subtitle.style.fontSize = '.7em';
    }

    increase(){
        console.log('Increase!');
    }

}
