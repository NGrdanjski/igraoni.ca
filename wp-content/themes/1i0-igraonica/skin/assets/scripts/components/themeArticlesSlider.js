import $ from 'jquery';
import Swiper from 'swiper';

export default class themeArticlesSlider {

    constructor(element){
        const elementClass = '.' + element.getAttribute('class');
        let themeArticlesSlider1 = new Swiper ('.swiper-container--big-theme', {
            direction: 'horizontal',
            loop: true,
            wrapperClass: 'swiper-wrapper--big-theme',
            slidesPerView: 5,
            spaceBetween: 30,
            pagination: {
                el: '.swiper-pagination2',
                type: 'bullets',
                clickable: true
            },
            autoplay: {
                delay: 4000,
            },
            breakpoints: {
                // when window width is  <= 320px
                375: {
                    slidesPerView: 1,
                    pagination: {
                        el: '.swiper-pagination2',
                        type: 'bullets',
                        clickable: true
                    },
                },
                // when window width is <= 480px
                480: {
                },
                // when window width is <= 640px
                640: {
                    slidesPerView: 1,
                }
            }
        });
    }

}