import $ from 'jquery';

export default class reorderComponent {
    constructor(element, component){
        let rElement = element;
        this.calculateWidth();
        this.reorder(element, component.target, component.width);
    }

    calculateWidth() {
        let width = window.innerWidth;
        return width;
    }

    reorder(e, t, w) {

        if (w >= this.calculateWidth())
        {
            $(e).insertBefore(t);
            $(e).addClass('sidebar-list--reordered');
        }
        else
        {
            $(e).insertBefore('.sidebar-list--featured');
            $(e).removeClass('sidebar-list--reordered');
        }
    }
}