import $ from 'jquery';
import Swiper from 'swiper';

export default class themeArticlesSlider3 {

    constructor(element){
        const elementClass = '.' + element.getAttribute('class');
        let themeArticlesSlider3 = new Swiper ('.swiper-container-ss', {
            direction: 'horizontal',
            loop: true,
            wrapperClass: 'swiper-wrapper-ss',
            slidesPerView: 1,
            spaceBetween: 0,
            autoplay: {
                delay: 10000,
            },
            effect: "fade",
            fadeEffect: {
                crossFade: true
            },
            pagination: {
                el: '.swiper-pagination-ss',
                type: 'progressbar',
            },
            breakpoints: {
                // when window width is  <= 320px
                375: {
                    slidesPerView: 1,
                },
                // when window width is <= 480px
                480: {
                },
                // when window width is <= 640px
                640: {
                    slidesPerView: 1,
                }
            }
        });

        // var autoplay = 5000;
        // var swiper = new Swiper('.swiper-container-ss', {
        //     pagination: '.swiper-pagination-ss',
        //     paginationClickable: true,
        //     watchSlidesProgress: true,
        //     autoplay: autoplay,
        //     onProgress: move
        // });
        // function move() {
        //     var elem = document.getElementById("progress");
        //     var width = 1;
        //     var autoplayTime = autoplay / 100;
        //     var id = setInterval(frame, autoplayTime);
        //     function frame() {
        //         if (width >= 100) {
        //             clearInterval(id);
        //         } else {
        //             width++;
        //             elem.style.width = width + '%';
        //         }
        //     }
        // }
    }

}