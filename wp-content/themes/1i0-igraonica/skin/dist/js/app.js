/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./skin/assets/scripts/app.js":
/*!************************************!*\
  !*** ./skin/assets/scripts/app.js ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

//import $ from 'jquery';
//window.$ = window.jQuery = $;
// import ScrollReveal from 'scrollreveal'
// const components = [
// ];
// components.forEach(component =>{
//     if (document.querySelector(component.selector) !== null)
//     {
//         document.querySelectorAll(component.selector).forEach(
//             element => new component.class(element, component.options)
//         );
//         if (component.options)
//         {
//             window.addEventListener("resize", () => {
//                 document.querySelectorAll(component.selector).forEach(
//                     element => new component.class(element, component.options)
//                 );
//             });
//         }
//     }
// });
$('.js-open-main-menu img').on('click', function () {
  $('.js-open-main-menu').toggleClass('header__main-menu--open');
  $('.js-open-second-menu').removeClass('footer__second-menu--open');
});
$('.js-open-second-menu img').on('click', function () {
  $('.js-open-second-menu').toggleClass('footer__second-menu--open');
  $('.js-open-main-menu').removeClass('header__main-menu--open');
});

/***/ }),

/***/ "./skin/assets/styles/application.scss":
/*!*********************************************!*\
  !*** ./skin/assets/styles/application.scss ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 0:
/*!********************************************************************************!*\
  !*** multi ./skin/assets/scripts/app.js ./skin/assets/styles/application.scss ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! /Users/nikolagrdanjski/Code/www/igraonica/igraoni.ca/wp-content/themes/1i0-igraonica/skin/assets/scripts/app.js */"./skin/assets/scripts/app.js");
module.exports = __webpack_require__(/*! /Users/nikolagrdanjski/Code/www/igraonica/igraoni.ca/wp-content/themes/1i0-igraonica/skin/assets/styles/application.scss */"./skin/assets/styles/application.scss");


/***/ })

/******/ });