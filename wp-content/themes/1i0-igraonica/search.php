<?php

$context = Timber::get_context();

$context['search_term'] = get_search_query();

//print_r($context['search_term']);

$templates = array( 'templates/pages/search.twig', 'archive.twig', 'index.twig' );



$queryPosts = new WP_Query( array(
    'post_type' => 'any',
    'posts_per_page' => 10,
    's' => $context['search_term']
));
$context['posts'] = new Timber\PostQuery($queryPosts);


// Latest news
$latest_posts = array(
    'post_type'         => 'post',
    'posts_per_page'    => '6',
);
$context['latest_posts'] = Timber::get_posts($latest_posts);

// random news
$random_posts = array(
    'post_type'         => 'post',
    'orderby' => 'rand',
    'posts_per_page'    => '7',
);
$context['rand_posts'] = Timber::get_posts($random_posts);

// izdvojeno news
$izdvojeno_posts = array(
    'post_type'         => 'post',
    'orderby' => 'rand',
    'posts_per_page'    => '7',
);
$context['izdvojeno_posts'] = Timber::get_posts($izdvojeno_posts);



Timber::render( $templates, $context );