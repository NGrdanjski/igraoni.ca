let mix = require('laravel-mix');

mix.js('skin/assets/scripts/app.js', 'skin/dist/js')
    .sass('skin/assets/styles/application.scss', 'skin/dist/css')
    .options({
        processCssUrls: false,
        postCss: [
            require('autoprefixer')({
                browsers: ['last 2 versions'],
                cascade: false
            })
        ]
    })
    .copyDirectory('skin/assets/fonts', 'skin/dist/fonts')
    .disableNotifications();
    // .copy('skin/assets/styles/fonts/FuturaPT-Medium.woff', 'skin/dist/css/FuturaPT-Medium.woff')
    // .copy('skin/assets/styles/fonts/FuturaPT-Medium.woff2', 'skin/dist/css/FuturaPT-Medium.woff2');
