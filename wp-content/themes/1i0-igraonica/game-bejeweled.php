<?php

$context = Timber::get_context();

function bodyClass($classes) {
	$classes[] = 'game-bejeweled page-games';
	return $classes;
}
add_filter('body_class', 'bodyClass');

Timber::render('templates/games/game-bejeweled.twig', $context);