<?php
use Roots\Sage\Setup;

// Check if Timber is not activated
if ( ! class_exists( 'Timber' ) ) {

    add_action( 'admin_notices', function() {
        echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php' ) ) . '</a></p></div>';
    } );
    return;

}

// Add the directory of templates in include path
Timber::$dirname = array('templates');

/**
 * Extend TimberSite with site wide properties
 */
class SageTimberTheme extends TimberSite {

    function __construct() {
        add_filter( 'timber_context', array( $this, 'add_to_context' ) );
        parent::__construct();
    }

    function add_to_context( $context ) {

        /* Menu */
        $context['main_menu'] = new \Timber\Menu( 'main_menu' );
        $context['footer_menu'] = new \Timber\Menu( 'footer_menu' );

        /* Site info */
        $context['site'] = $this;

        $context['images'] = get_template_directory_uri() . '/skin/assets/images';

        // $context['image_cdn'] = 'http://d2ii67dd5ulhyn.cloudfront.net/wp-content/themes/1i0-igraonica/';

        // category
        $cat_terms = get_terms('category',
            array(
                'parent' => 0,
                'orderby' => 'slug',
                'hide_empty' => false
            )
        );

        foreach($cat_terms as $term)
        {
            $old_slug = $term->slug;
            $new_slug = str_replace("-", "_", $old_slug);
            $context[$new_slug] = new TimberTerm($old_slug);
        }

        /* Sidebar */

        // Latest news
        $latest_posts = array(
            'post_type'         => 'post',
            'posts_per_page'    => '6',
        );
        $context['latest_posts'] = Timber::get_posts($latest_posts);

        // random news
        $random_posts = array(
            'post_type'         => 'post',
            'orderby' => 'rand',
            'posts_per_page'    => '10',
        );
        $context['rand_posts'] = Timber::get_posts($random_posts);

        // izdvojeno news
        $izdvojeno_posts = array(
            'post_type'         => 'post',
            'orderby' => 'rand',
            'posts_per_page'    => '7',
        );
        $context['izdvojeno_posts'] = Timber::get_posts($izdvojeno_posts);



        return $context;
    }
}
new SageTimberTheme();
