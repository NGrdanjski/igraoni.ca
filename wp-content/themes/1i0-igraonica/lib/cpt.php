<?php

/**
 * Registered Custom Post Types
 * @author NGrdanjski
 */
function registerCustomPostsTypes() {

    /**
     * Post Type: Products.
     */

	$labels = array(
		"name" => __( "Products", "" ),
		"singular_name" => __( "Product", "" ),
        'add_new_item'          => __( 'Add New Product', 'text_domain' ),
        'add_new'               => __( 'Add New Product', 'text_domain' ),
        'new_item'              => __( 'New Product', 'text_domain' ),
        'edit_item'             => __( 'Edit Product', 'text_domain' ),
        'update_item'           => __( 'Edit Product', 'text_domain' ),
        'view_item'             => __( 'View Product', 'text_domain' ),
        'view_items'            => __( 'View Products', 'text_domain' ),
        'search_items'          => __( 'Search Products', 'text_domain' ),
	);

	$args = array(
		"label" => __( "Products", "" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => true,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => true,
		"rewrite" => array(
			"slug" => "proizvod",
			"with_front" => true
		),
		"query_var" => true,
		"supports" => array(
			"title",
			"editor",
			"thumbnail",
			"page-attributes"
		),
        "menu_icon" => "dashicons-cart",
	);

	register_post_type( "products", $args );


	/**
	 * Post Type: Games.
	 */

	// $labels = array(
	// 	"name" => __( "Games", "" ),
	// 	"singular_name" => __( "Game", "" ),
	// 	'add_new_item'          => __( 'Add New Game', 'text_domain' ),
	// 	'add_new'               => __( 'Add New Game', 'text_domain' ),
	// 	'new_item'              => __( 'New Game', 'text_domain' ),
	// 	'edit_item'             => __( 'Edit Game', 'text_domain' ),
	// 	'update_item'           => __( 'Edit Game', 'text_domain' ),
	// 	'view_item'             => __( 'View Game', 'text_domain' ),
	// 	'view_items'            => __( 'View Game', 'text_domain' ),
	// 	'search_items'          => __( 'Search Game', 'text_domain' ),
	// );

	// $args = array(
	// 	"label" => __( "Games", "" ),
	// 	"labels" => $labels,
	// 	"description" => "",
	// 	"public" => true,
	// 	"publicly_queryable" => false,
	// 	"show_ui" => true,
	// 	"show_in_rest" => false,
	// 	"rest_base" => "",
	// 	"has_archive" => false,
	// 	"show_in_menu" => true,
	// 	"exclude_from_search" => false,
	// 	"capability_type" => "post",
	// 	"map_meta_cap" => true,
	// 	"hierarchical" => true,
	// 	"rewrite" => array(
	// 		"slug" => "igraj-se",
	// 		"with_front" => true
	// 	),
	// 	"query_var" => true,
	// 	"supports" => array(
	// 		"title",
	// 		"editor",
	// 		"thumbnail",
	// 		"page-attributes"
	// 	),
	// 	"menu_icon" => "dashicons-image-filter",
	// );

	// register_post_type( "games", $args );


}

add_action( 'init', 'registerCustomPostsTypes' );



/**
 * Registered Custom Post Taxonomy
 * @author NGrdanjski
 */
// function registerCustomTaxonomyTypes() {

    /**
     * Taxonomy: Tip.
     */

//	$labels = array(
//		"name" => __( "Tipovi Filmova", "" ),
//		"singular_name" => __( "Tip", "" ),
//	);
//
//	$args = array(
//		"label" => __( "Tip", "" ),
//		"labels" => $labels,
//		"public" => true,
//		"hierarchical" => true,
//		"label" => "Tip",
//		"show_ui" => true,
//		"show_in_menu" => true,
//		"show_in_nav_menus" => true,
//		"query_var" => true,
//		"rewrite" => array( 'slug' => 'tip', 'with_front' => true, ),
//		"show_admin_column" => true,
//		"show_in_rest" => false,
//		"rest_base" => "",
//		"show_in_quick_edit" => false,
//	);
//	register_taxonomy( "Tip", array( "filmovi" ), $args );
// }

// add_action( 'init', 'registerCustomTaxonomyTypes' );