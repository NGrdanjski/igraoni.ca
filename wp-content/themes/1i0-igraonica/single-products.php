<?php
/**
 * Single page template
 *
 * @package  WordPress
 * @subpackage  SageTimber
 * @since  SageTimber 0.1
 */

$context = Timber::get_context();
$context['post'] = new Timber\Post();

function bodyClass($classes) {
	$classes[] = 'time-machine-page';
	return $classes;
}
add_filter('body_class', 'bodyClass');


// products
$products = array(
    'post_type'         => 'products',
    'orderby' => 'rand',
    'posts_per_page'    => '20',
);
$context['products'] = Timber::get_posts($products);

Timber::render('single/single-products.twig', $context);