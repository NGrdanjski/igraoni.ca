<?php
/*
 * Template Name: Games
 */

$context = Timber::get_context();
$context['post'] = new Timber\Post();

function bodyClass($classes) {
    $classes[] = 'page-games';
    return $classes;
}
add_filter('body_class', 'bodyClass');

Timber::render('templates/template-games-page.twig', $context);